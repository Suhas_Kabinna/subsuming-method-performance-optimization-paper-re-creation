#ifndef METHOD
#define METHOD

#include <iostream>
#include "Node.h"

using namespace std;

class Node;

class Method {
	public:
		int getMaxHeight();
		int getMinCPD();
		int getInduced();
		Node *getFirstNode();
		std::list<Node *>& getAllNodes();
		string getMethodName();
		string getClassName();
		string getMethodSig();
		Method *getCommonParent();

		void setMaxHeight(int height);
		void setMinCPD(int CPD);
		void setInduced(int induced);
		void insertNode(Node *pNode);
		void setMethodName(string lMethodName);
		void setClassName(string lClassName);
		void setMethodSig(string lMethodSig);
		void setCommonParent(Method *lMethod);

	private:
		std::list<Node *> nodes;
		int maxHeight;
		int minCPD;
		int induced;
		string methodName;
		string className;
		string methodSig;
		Method *commonParent;
};

#endif
