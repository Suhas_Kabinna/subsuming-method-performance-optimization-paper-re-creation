CPPFLAGS=-g  
LDFLAGS=-g
LDLIBS=

main: main.o tree.o node.o method.o parse_profiler.o pugixml.o
	g++ $(LDFLAGS) -o main Main.o Tree.o Node.o Method.o parse_profiler.o pugixml.o $(LDLIBS) 

main.o: Main.cpp Main.h
	g++ $(CPPFLAGS) -c Main.cpp

tree.o: Tree.cpp Tree.h
	g++ $(CPPFLAGS) -c Tree.cpp

node.o: Node.cpp Node.h
	g++ $(CPPFLAGS) -c Node.cpp

method.o: Method.cpp Method.h
	g++ $(CPPFLAGS) -c Method.cpp
parse_profiler.o: parse_profiler.cpp 
	g++ $(CPPFLAGS) -c parse_profiler.cpp

pugixml.o: pugixml.cpp
	g++ $(CPPFLAGS) -c pugixml.cpp
