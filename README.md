Recreation of paper < https://www.cs.auckland.ac.nz/~dmap001/sma-cs.pdf > for performance analysis, using XML parse_profiler.

The data is collected from running Junit on Pet-clinic application for 2 hours and the XML file parsed to identify hotspots for performance analysis.

Repository also contains the paper, which explains each of the steps in depth.