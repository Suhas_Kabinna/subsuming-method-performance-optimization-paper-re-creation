#ifndef TREE
#define TREE

#include <iostream>
#include "Node.h"
#include "Method.h"
#include "pugixml.hpp"

class Node;
class Method;

class Tree {
	public:
		Node *getRoot();
		std::list <Method*>& getMethods();

		void setRoot(Node *pRoot);
		void insertMethod(Method *pMethod);
		void calculateHeight(Node *v); 
		void calculateInducedCost(Node *n);
		void parse_profiler(char* profiler_file);
		void dumpTree();
		int CPD(Method *p,Method *m);
		void MinCPD(Method *m);
		int DISTANCE(Method *m,Node *n);
		
	private:
		void parse_node(Node *CCTNode, pugi::xml_node node);
		void dumpNode(Node* CCTNode);
		Node *root;
		std::list <Method*> methods;
};

#endif
