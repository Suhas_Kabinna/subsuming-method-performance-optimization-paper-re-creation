#include "Node.h"

Method *Node::getMethod() {
	return method;
}

int Node::getCost() {
	return cost;
}

Node *Node::getParent() {
	return parent;
}

list<Node *>& Node::getAllChildren() {
	return children;
}

Node *Node::getAdjustedParent() {
	return adjustedParent;
}

list<Node*>& Node::getAllAdjustedChildren() {
	return adjustedChildren;
}

int Node::getHeight() {
	return height;
}

int Node::getInduced() {
	return induced;
}

void Node::setMethod(Method *pMethod) {
	method = pMethod;
}

void Node::setCost(int lcost) {
	cost = lcost;
}

void Node::setParent(Node *pParent) {
	parent = pParent;
}

void Node::insertChildren(Node *pChildren) {
	children.push_back(pChildren);
}

void Node::setAdjustedParent(Node *pAdjustedParent) {
	adjustedParent = pAdjustedParent;
}

void Node::insertAdjustedChildren(Node *pChildren) {
	adjustedChildren.push_back(pChildren);
}

void Node::setHeight(int lheight) {
	height = lheight;
}

void Node::setInduced(int linduced) {
	induced = linduced;
}

