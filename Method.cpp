#include "Method.h"

int Method::getMaxHeight() {
	return maxHeight;
}

int Method::getMinCPD() {
	return minCPD;
}

int Method::getInduced() {
	return induced;
}

Node *Method::getFirstNode() {
	return nodes.front();
}

std::list<Node *>& Method::getAllNodes() {
	return nodes;
}

void Method::setMaxHeight(int height) {
	maxHeight = height;
}

void Method::setMinCPD(int CPD) {
	minCPD = CPD;
}

void Method::setInduced(int ind) {
	induced = ind;
}

void Method::insertNode(Node *pNode) {
	nodes.push_back(pNode);
}


string Method::getMethodName() {
	return methodName;
}

string Method::getClassName() {
	return className;
}

string Method::getMethodSig() {
	return methodSig;
}

Method *Method::getCommonParent() {
	return commonParent;
}

void Method::setMethodName(string lMethodName) {
	methodName = lMethodName;
}

void Method::setClassName(string lClassName) {
	className = lClassName;
}

void Method::setMethodSig(string lMethodSig) {
	methodSig = lMethodSig;
}

void Method::setCommonParent(Method *lMethod) {
	commonParent = lMethod;
}

