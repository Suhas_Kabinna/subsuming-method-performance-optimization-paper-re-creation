#ifndef NODE
#define NODE

#include <iostream>
#include <list>
#include "Method.h"

class Method;

class Node {
	public:
		Method *getMethod();
		int getCost();
		Node *getParent();
		std::list<Node *>& getAllChildren();
		Node *getAdjustedParent();
		std::list<Node*>& getAllAdjustedChildren();
		int getHeight();
		int getInduced();

		void setMethod(Method *pMethod);
		void setCost(int lcost);
		void setParent(Node *pParent);
		void insertChildren(Node *pChildren);
		void setAdjustedParent(Node *pAdjustedParent);
		void insertAdjustedChildren(Node *pChildren);
		void setHeight(int lheight);
		void setInduced(int linduced);

	private:
		Method *method;
		int cost;
		Node *parent;
		std::list<Node *> children;
		Node *adjustedParent;
		std::list<Node*> adjustedChildren;
		int height;
		int induced;
};

#endif
