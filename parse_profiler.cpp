#include "pugixml.hpp"
#include "Tree.h"
#include "Method.h"
#include "Node.h"

#include <string.h>
#include <iostream>

using std::string;


// void parse_profiler(string profiler_file);
// void parse_node(pugi::xml_node node);

// int main()
// {
//   parse_profiler("Call_Tree_one_hour.xml");
//   return 0;
// }

void Tree::parse_profiler(char* profiler_file)
{
  pugi::xml_document doc; 
  if (!doc.load_file(profiler_file))
    std::cout << "Failed to load the xml file!" << std::endl;
  
  // the root of the CCT, there is no specific method related to the node since there is no common entry for all the methods
  Node *rootNode = new Node; 
  rootNode->setMethod(NULL);
  rootNode->setCost(0);
  rootNode->setParent(NULL);
  rootNode->setAdjustedParent(NULL);
  rootNode->insertAdjustedChildren(NULL);
  rootNode->setHeight(0);
  rootNode->setInduced(0);

  setRoot(rootNode);

  pugi::xml_node tree = doc.child("tree");
  for (pugi::xml_node node  = tree.first_child(); node; node = node.next_sibling()) {
    // for (pugi::xml_attribute attr = node.first_attribute(); attr; attr = attr.next_attribute()) {
    //   std::cout << " " << attr.name() << "=" << attr.value();
    // }
    Node *cNode = new Node;
    rootNode->insertChildren(cNode);
    cNode -> setParent(rootNode);
    parse_node(cNode, node);
  }

}

void Tree::parse_node(Node *CCTNode, pugi::xml_node node)
{
  string methodName =  node.attribute("methodName").value();
  string className =  node.attribute("class").value();
  string signature =  node.attribute("methodSignature").value();
  int leaf =  node.attribute("leaf").as_int();
  int cost =  node.attribute("inherentTime").as_int();
  
  std::list<Method*>::const_iterator iterator;
  int exist = 0;
  Method *method;
  for (iterator = methods.begin(); iterator != methods.end(); ++iterator) {
    if ((*iterator)->getMethodName() == methodName &&
	(*iterator)->getClassName() == className &&
	(*iterator)->getMethodSig() == signature) {
      exist = 1;
      method = *iterator;
      method->insertNode(CCTNode);
    }
  }

  if (!exist) {
    // create a new method
    method = new Method;
    method->setMethodName(methodName);
    method->setClassName(className);
    method->setMethodSig(signature);
    method->insertNode(CCTNode);
    
    // insert the method to the methods list of the tree
    insertMethod(method);
  }
    
  // set the method of the node
  CCTNode->setMethod(method);
  CCTNode->setCost(cost);

  for (pugi::xml_node childNode  = node.first_child(); childNode; childNode = childNode.next_sibling()) {
    Node *cNode = new Node;
    CCTNode->insertChildren(cNode);
    cNode -> setParent(CCTNode);
    parse_node(cNode, childNode);
  }
}

void Tree::dumpTree()
{
  std::cout << "Methods List: " << std::endl;
  std::list<Method*>::const_iterator iterM;
  for (iterM = methods.begin(); iterM != methods.end(); ++iterM) {
    std::cout << "method: " << (*iterM)->getMethodName() 
	      << "; class: " << (*iterM)->getClassName()
	      << "; signature: " << (*iterM)->getMethodSig()
	      << std::endl;
  }

  std::cout << "Nodes List: " << std::endl;
  std::list<Node*>::const_iterator iterN;
  std::list<Node*> children = root->getAllChildren();
  for (iterN = children.begin(); iterN != children.end(); ++iterN) {
    dumpNode(*iterN);
  }
  
}

void Tree::dumpNode(Node* CCTNode)
{
  Method *method = CCTNode->getMethod();

  std::cout << "method: " << method->getMethodName()
       << "; class: " << method->getClassName()
       << "; signature: " << method->getMethodSig()
       << std::endl;
  std::cout << "cost: " << CCTNode->getCost() << std::endl;
  
  std::list<Node*>::const_iterator iterN;
  std::list<Node*> children = CCTNode->getAllChildren();
  for (iterN = children.begin(); iterN != children.end(); ++iterN) {
    dumpNode(*iterN);
  }

}
