#include "Tree.h"

bool isSubsumedMethod(Method *pMethod) {
	return false;
}

Node *Tree::getRoot() {
	return root;
}

list <Method*>& Tree::getMethods() {
	return methods;
}

void Tree::setRoot(Node *pRoot) {
	root = pRoot;
}

void Tree::insertMethod(Method *pMethod) {
	methods.push_back(pMethod);
}

void Tree::calculateHeight(Node *v) {
	v->setHeight(0);	
	std::list<Node *> adjustedChildren = v->getAllAdjustedChildren();
	std::list<Node *>::const_iterator it;
	for (it = adjustedChildren.begin(); it != adjustedChildren.end(); ++it) {
		Node *child = *it;
		calculateHeight(child);
		if (child->getHeight() > v->getHeight()) {
			v->setHeight(child->getHeight() + 1);
		}
	}

	if(v->getHeight() > v->getMethod()->getMaxHeight()) {
		v->getMethod()->setMaxHeight(v->getHeight());
	}
} 


void Tree::calculateInducedCost(Node *n) {
	n->setInduced(n->getCost());
	std::list<Node *> children = n->getAllChildren();
	std::list<Node *>::const_iterator it;
	for (it = children.begin(); it != children.end(); ++it) {
		Node *child = *it;
    		calculateInducedCost(child);
		if(isSubsumedMethod(child->getMethod())) {
			n->setInduced(n->getInduced() + child->getInduced());	
		}
	}
	n->getMethod()->setInduced(n->getMethod()->getInduced() + n->getInduced());
}
	

void Tree::MinCPD(Method *m)
{
	m->setMinCPD(10000);
	int distance;
	Node* n;
	n = m->getFirstNode();
	n = n->getAdjustedParent();

	while (n != NULL){
		distance = CPD(n->getMethod(),m);
		if (distance < m->getMinCPD()){
			m->setMinCPD(distance);
			Method *child;
			child = n->getMethod();
			m->setCommonParent(child);
		}

		n = n->getParent();
	}
}


int Tree::CPD(Method *p,Method *m)
{
	int cpd=0,dist;
        std::list<Node *> n = m->getAllNodes();
        std::list<Node *>::const_iterator it;
        for (it = n.begin(); it != n.end(); ++it) {
		Node *Temp = *it;
		dist = DISTANCE(p,Temp);
		if (dist > cpd) {
			cpd = dist;
		}

	}

	return cpd;

}

int Tree::DISTANCE(Method *m,Node *n)
{
	int dist = 0;
	while (1) {
		dist ++ ;
		n = n->getAdjustedParent();
		if (n == NULL){
			return 10000;
		}
		if (n->getMethod() == m ){
			return dist;
		}

	}



}

